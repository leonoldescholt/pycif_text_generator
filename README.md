# Pycif text generator

#### Example text as SVG

<img src="img/Pycif_Text.png" alt="text component screenshot" width="400"/>


#### Options
The function creates a library consisting out of the modern latic alphabeth (only capitals) the numbers and a selection of symbols.
Using the function requires the use of the "string":"example text" option. The size of the characters can be changed using the "mag":1 option.

Good luck!

#### Instalation and use

The following code can be used in Jupyter notebook to install the pycif text generator

*Installation*
```
%pip install git+https://gitlab.tudelft.nl/leonoldescholt/pycif_text_generator.git
```

*Use*
```
import pc_text
from pc_text import Text

my_text = Text({
    "string":"pycif text", "mag":1
})
my_text

```

#### Acceptance
pc_Text does not accepts all symbols. Below is a list of the characters that are excepted.

**Modern Latin** 
(only uppercase) <br>
`A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z` <br>
lowercase is converted to uppercase

**Numerals**
`1,2,3,4,5,6,7,8,9,0`

**Symbols**
`
~,!,@,#,$,%,^,&,*,(,),_,+,{,},|, ,:,",<,>,?,-,=,[,],;,',,,.,/,
`


#### Notes
- The backslash symbol `\` is not included as a symbol.
- When using the quotation mark `"` or the apostrophe `'` make sure to define the string with the other. Both can not be used in a single string.


